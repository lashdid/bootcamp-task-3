import { CustomAppBar } from "./components/CustomAppBar";
import { Content } from "./components/Content";
import { CustomModal } from "./components/CustomModal";
import { useState } from "react";
import { ListContent } from "./components/ListContent";

export default function App() {
  const [isModalOpen, setModalOpen] = useState(false);
  const [dataId, setDataId] = useState();
  const [data, setData] = useState([]);
  return (
    <main>
      <CustomAppBar onButtonClick={() => setModalOpen(true)} />
      {data.length === 0 ? <Content /> : <ListContent listArray={data} onEdit={(id) => {
        setModalOpen(true)
        setDataId(id)
      }}/>}
      <CustomModal
        isModalOpen={isModalOpen}
        onModalClose={() => {
          setModalOpen(false)
          setDataId()
        }}
        editValue={dataId && data.filter((item) => item.id === dataId)[0]}
        onFormSave={(item) => {
          if(item.id === dataId){
            const newData = data.map((oldData) => {
              if(oldData.id === item.id){
                return item
              }
              return oldData
            })
            setData(newData)
            setModalOpen(false)
            setDataId()
          }
          else{
            setData([...data, item]);
            setModalOpen(false);
          }
        }}
      />
    </main>
  );
}