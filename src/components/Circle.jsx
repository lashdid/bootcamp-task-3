import React from "react";

export const Circle = () => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: "200px",
        height: "200px",
        backgroundColor: "#f7b531",
        border: "1px black solid",
        borderRadius: "100%",
      }}
    >
      <div
        style={{
          width: "100px",
          height: "120px",
          backgroundColor: "white",
          border: "1px black solid",
          borderRadius: "150px",
        }}
      />
    </div>
  );
};
