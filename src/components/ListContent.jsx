import { Box, Card, Typography, TextField, Button } from '@mui/material'
import React, { useState } from 'react'

export const ListContent = ({listArray, onEdit}) => {
  const [search, setSearch] = useState('')

  const filteredArray = listArray.filter((item) => item.name.toLowerCase().includes(search.toLowerCase()))

  return (
    <Box style={{display: 'flex', flexDirection: 'column', gap: '1rem'}}>
      <TextField
        style={{margin: '1rem'}}
        placeholder='Search...'
        variant='outlined'
        value={search}
        onChange={(e) => setSearch(e.target.value)}
      />
      {filteredArray.map((item, idx) => (
        <Card key={idx} style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '1rem'}}>
          <div>
            <Typography variant="h4" color="initial">{item.name}</Typography>
            <Typography variant="h6" color="initial">{item.address}</Typography>
          </div>
          <div style={{display: 'flex', gap: '1rem'}}>
            <Typography variant="h4" color="initial">{item.hobby}</Typography>
            <Button onClick={() => onEdit(item.id)} variant='contained' style={{paddingInline: '2rem', backgroundColor: '#00c2cb', fontSize: '18px', textTransform: 'none', borderRadius: '50px'}}>Edit</Button>
          </div>
        </Card>
      ))}
    </Box>
  )
}
