import { Button, TextField } from "@mui/material";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import { useEffect, useState } from "react";

export const CustomModal = ({ isModalOpen, onModalClose, onFormSave, editValue }) => {
  const [data, setData] = useState({
    id: 0,
    name: "",
    address: "",
    hobby: "",
  });

  useEffect(() => {
    editValue && setData(editValue)
  }, [editValue])

  const [isSubmit, setSubmit] = useState(false);

  const reset = () => {
    setSubmit(false);
    setData({ id: 0, name: "", address: "", hobby: "" });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    const id = editValue ? editValue.id : Math.floor(Math.random() * 10000)
    setData({ ...data, id, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setSubmit(true);
    let isEmpty = Object.values(data).some((value) => value === "");
    if (!isEmpty) {
      onFormSave(data);
      reset();
    }
  };
  return (
    <Modal
      open={isModalOpen}
      onClose={() => {
        onModalClose();
        reset();
      }}
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <form
        style={{
          minWidth: "20rem",
          display: "flex",
          flexDirection: "column",
          gap: "1rem",
          backgroundColor: "white",
          padding: "2rem",
          borderRadius: "1rem",
        }}
        onSubmit={handleSubmit}
      >
        <Typography variant="h4" align="center">
          Add User
        </Typography>
        <TextField
          error={isSubmit && data.name === ""}
          name="name"
          value={data.name}
          onChange={handleChange}
          fullWidth
          label="Name"
          variant="outlined"
        />
        <TextField
          error={isSubmit && data.address === ""}
          name="address"
          value={data.address}
          onChange={handleChange}
          fullWidth
          label="Address"
          variant="outlined"
        />
        <TextField
          error={isSubmit && data.hobby === ""}
          name="hobby"
          value={data.hobby}
          onChange={handleChange}
          fullWidth
          label="Hobby"
          variant="outlined"
        />
        <Button
          variant="contained"
          type="submit"
          style={{
            alignSelf: "center",
            paddingInline: "3rem",
            color: "black",
            backgroundColor: "#00c2cb",
            fontSize: "18px",
            textTransform: "none",
            borderRadius: "50px",
          }}
        >
          Save
        </Button>
      </form>
    </Modal>
  );
};
